<?php

namespace Drupal\commerce_shipping_mondial_relay\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Provides the Mondial relay information pane.
 *
 * Collects the mondial relay profile. Assumes shipping_information exists.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_shipping_mondial_relay",
 *   label = @Translation("Mondial Relay Widget"),
 *   default_step = "shipping_information",
 * )
 */
class MondialRelay extends CheckoutPaneBase implements CheckoutPaneInterface, ContainerFactoryPluginInterface {

  /**
   * Provide default widget settings.
   *
   * @var string[]
   */
  protected $widgetSettings = [
    'Country' => '',
    'PostCode' => '',
    'City' => '',
  ];

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $shipping_method = $this->getShippingMethodPluginId($form_state);
    if ('commerce_shipping_mondial_relay' !== $shipping_method) {
      return [];
    }

    $this->getWidgetSettings($pane_form, $form_state);

    $pane_form['widget'] = [
      '#type' => 'container',
      '#id' => 'commerce_shipping_mondial_relay_widget',
      '#attached' => [
        'library' => [
          'commerce_shipping_mondial_relay/mondialrelay',
        ],
        'drupalSettings' => [
          'commerce_shipping_mondial_relay' => [
            'widget' => $this->widgetSettings,
          ],
        ],
      ],
    ];
    $pane_form['pickup'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      'pickup_id' => [
        '#type' => 'hidden',
        '#id' => 'commerce-shipping-mondial-relay-pickup-id',
      ],
      'pickup_selected' => [
        '#type' => 'container',
        '#id' => 'commerce_shipping_mondial_relay__pickup__selected',
      ],
      'pickup_address' => [
        '#type' => 'container',
        '#id' => 'commerce_shipping_mondial_relay__pickup__address',
        'id' => [
          '#type' => 'hidden',
          '#id' => 'commerce_shipping_mondial_relay__pickup__address__id',
        ],
        'nom' => [
          '#type' => 'hidden',
          '#id' => 'commerce_shipping_mondial_relay__pickup__address__nom',
        ],
        'adresse1' => [
          '#type' => 'hidden',
          '#id' => 'commerce_shipping_mondial_relay__pickup__address__adresse1',
        ],
        'adresse2' => [
          '#type' => 'hidden',
          '#id' => 'commerce_shipping_mondial_relay__pickup__address__adresse2',
        ],
        'cp' => [
          '#type' => 'hidden',
          '#id' => 'commerce_shipping_mondial_relay__pickup__address__cp',
        ],
        'ville' => [
          '#type' => 'hidden',
          '#id' => 'commerce_shipping_mondial_relay__pickup__address__ville',
        ],
        'pays' => [
          '#type' => 'hidden',
          '#id' => 'commerce_shipping_mondial_relay__pickup__address__pays',
        ],
      ],
      'pickup_info' => [
        '#theme' => 'commerce_shipping_mondial_relay__widget__info',
      ],
    ];
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(
    array &$pane_form,
    FormStateInterface $form_state,
    array &$complete_form,
  ) {
    $keys = array_merge($pane_form['#parents'], ['pickup', 'pickup_id']);
    $pickup_id = $form_state->getValue($keys);
    if (empty($pickup_id) && $form_state->hasValue($keys)) {
      $form_state->setError($pane_form['widget'], $this->t('You have to select a pick up'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(
    array &$pane_form,
    FormStateInterface $form_state,
    array &$complete_form,
  ) {
    $shipping_method = $this->getShippingMethodPluginId($form_state);
    if ('commerce_shipping_mondial_relay' !== $shipping_method) {
      return [];
    }

    // Save custom profile owned by anonymous with pickup informations.
    $keys = array_merge($pane_form['#parents'], ['pickup', 'pickup_id']);
    $pickup_id = $form_state->getValue($keys);
    if (empty($pickup_id)) {
      return [];
    }

    $profileStorage = $this->entityTypeManager->getStorage('profile');
    $properties = [
      'type' => 'mondial_relay',
      'field_pickup_id' => $pickup_id,
    ];
    $profiles = $profileStorage->loadByProperties($properties);
    if (empty($profiles)) {
      $properties['uid'] = 0;
      $keys = array_merge($pane_form['#parents'], ['pickup', 'pickup_address']);
      $address = $form_state->getValue($keys);
      if ($address) {
        $properties['field_pickup_address'] = [
          'organization' => $address['nom'],
          'address_line1' => $address['adresse1'],
          'address_line2' => $address['adresse2'],
          'postal_code' => $address['cp'],
          'locality' => $address['ville'],
          'country_code' => $address['pays'],
        ];
      }
      $profile = $profileStorage->create($properties);
      $profile->save();
    }
    else {
      $profile = reset($profiles);
    }

    // Save the modified shipments.
    $shipments = [];
    if (!$this->order->shipments->isEmpty()) {
      foreach ($this->order->shipments->referencedEntities() as $shipment) {
        $shipment->setShippingProfile($profile);
        $shipments[] = $shipment;
      }
      $this->order->shipments = $shipments;
    }
  }

  /**
   * Get the current shipping method plugin id selected in checkout.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return string|null
   *   Return the shipping method plugin id or null otherwise.
   */
  protected function getShippingMethodPluginId(FormStateInterface $form_state): ?string {
    if ($shippingMethod = $this->getShippingMethodPlugin($form_state)) {
      return $shippingMethod->getPluginId();
    }
    return NULL;
  }

  /**
   * Get the current shipping method plugin selected in checkout.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return \Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodInterface|null
   *   Return the shipping method plugin or null otherwise.
   */
  protected function getShippingMethodPlugin(FormStateInterface $form_state): ?ShippingMethodInterface {
    $keys = ['shipping_information', 'shipments', 0, 'shipping_method'];
    if ($form_state->hasValue($keys)) {
      $shipping_method_value = reset($form_state->getValue($keys));
      [$shipping_method_id] = explode('--', $shipping_method_value);
      $shipping_method = \Drupal::service('entity_type.manager')->getStorage('commerce_shipping_method')->load($shipping_method_id);
      if (NULL !== $shipping_method) {
        return $shipping_method->getPlugin();
      }
    }

    if (empty($this->order->shipments)) {
      return NULL;
    }

    $shipments = $this->order->shipments->referencedEntities();
    $shipment = reset($shipments);
    if (!$shipment) {
      return NULL;
    }

    $shipping_methods = $shipment->shipping_method->referencedEntities();
    if (empty($shipping_methods)) {
      return NULL;
    }

    /** @var \Drupal\commerce_shipping\Entity\ShippingMethodInterface $shipping_method */
    $shipping_method = reset($shipping_methods);
    if (!$shipping_method) {
      return NULL;
    }

    return $shipping_method->getPlugin();
  }

  /**
   * Get the shipping profile from current order.
   *
   * @return \Drupal\profile\Entity\ProfileInterface|null
   *   The shipping profile, NULL otherwise.
   */
  protected function getShippingProfile(): ?ProfileInterface {
    if (isset($this->order->shipments)) {
      $shipments = $this->order->shipments->referencedEntities();
      if (empty($shipments)) {
        return NULL;
      }
      $shipment = reset($shipments);
      $shipment_profiles = $shipment->shipping_profile->referencedEntities();
      if (empty($shipment_profiles)) {
        return NULL;
      }
      return reset($shipment_profiles);
    }
    return NULL;
  }

  /**
   * Get widget address settings thanks to profile.
   *
   * @param array $pane_form
   *   The pane form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return void
   *   Nothing to return.
   */
  protected function getWidgetSettings(array $pane_form, FormStateInterface $form_state): void {
    $shippingProfile = $this->getShippingProfile();
    if (NULL === $shippingProfile) {
      return;
    }

    if (!$shippingProfile->hasField('address') && !$shippingProfile->hasField('field_pickup_address')) {
      return;
    }

    $address_field = $shippingProfile->hasField('address') ? 'address' : 'field_pickup_address';

    $addresses = $shippingProfile->get($address_field)->getValue();
    $address = reset($addresses);
    if ($address) {
      $this->widgetSettings['Country'] = $address['country_code'];
      $this->widgetSettings['PostCode'] = $address['postal_code'];
      $this->widgetSettings['City'] = $address['locality'];
    }
    if ($shippingProfile->hasField('field_pickup_id') && !$shippingProfile->get('field_pickup_id')->isEmpty()) {
      $this->widgetSettings['AutoSelect'] = $shippingProfile->get('field_pickup_id')->getString();
    }

    $shippingMethod = $this->getShippingMethodPlugin($form_state);
    $shippingMethodConfiguration = $shippingMethod->getConfiguration();

    if (empty($this->widgetSettings['Country'])) {
      $this->widgetSettings['Country'] = $shippingMethodConfiguration['widget']['default_country'];
    }
    $this->widgetSettings['Brand'] = $shippingMethodConfiguration['widget']['brand'];
    $this->widgetSettings['EnableGeolocalisatedSearch'] = (bool) $shippingMethodConfiguration['widget']['enable_geolocalisated_search'];
    $this->widgetSettings['ColLivMod'] = $shippingMethodConfiguration['widget']['col_liv_mod'];
    $this->widgetSettings['NbResults'] = $shippingMethodConfiguration['widget']['nb_results'];
    $this->widgetSettings['SearchDelay'] = $shippingMethodConfiguration['widget']['search_delay'];
    $this->widgetSettings['MapScrollWheel'] = (bool) $shippingMethodConfiguration['widget']['map_scroll_wheel'];
    $this->widgetSettings['MapStreetView'] = (bool) $shippingMethodConfiguration['widget']['map_street_view'];
    $this->widgetSettings['Responsive'] = (bool) $shippingMethodConfiguration['widget']['responsive'];
    $this->widgetSettings['ShowResultsOnMap'] = (bool) $shippingMethodConfiguration['widget']['show_results_on_map'];
    $this->widgetSettings['DisplayMapInfo'] = (bool) $shippingMethodConfiguration['widget']['display_map_info'];
    $this->widgetSettings['Theme'] = $shippingMethodConfiguration['widget']['theme'];

    if (NULL !== $this->order) {
      $store = $this->order->getStore();
      $available_countries = [];
      foreach ($store->get('shipping_countries') as $country_item) {
        $available_countries[] = $country_item->value;
      }
      if (!empty($available_countries)) {
        $this->widgetSettings['AllowedCountries'] = implode(',', $available_countries);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    if (!$this->order->hasField('shipments')) {
      return FALSE;
    }

    // The order must contain at least one shippable purchasable entity.
    foreach ($this->order->getItems() as $order_item) {
      $purchased_entity = $order_item->getPurchasedEntity();
      if ($purchased_entity && $purchased_entity->hasField('weight')) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
