<?php

namespace Drupal\commerce_shipping_mondial_relay\Plugin\Commerce\ShippingMethod;

use CommerceGuys\Addressing\Country\CountryRepositoryInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\Core\Form\FormStateInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Mondial Relay shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "commerce_shipping_mondial_relay",
 *   label = @Translation("Mondial Relay"),
 * )
 */
class MondialRelay extends ShippingMethodBase {

  /**
   * The country repository.
   *
   * @var \CommerceGuys\Addressing\Country\CountryRepositoryInterface
   */
  protected $countryRepository;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \CommerceGuys\Addressing\Country\CountryRepositoryInterface $countryRepository
   *   The country repository.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PackageTypeManagerInterface $package_type_manager,
    WorkflowManagerInterface $workflow_manager,
    CountryRepositoryInterface $countryRepository,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);
    $this->services['default'] = new ShippingService('default', $this->configuration['rate_label']);
    $this->countryRepository = $countryRepository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('address.country_repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'rate_label' => '',
      'rate_description' => '',
      'rate_amount' => NULL,
      'widget' => [
        'brand' => NULL,
        'default_country' => [],
        'enable_geolocalisated_search' => NULL,
        'col_liv_mod' => '24R',
        'nb_results' => 7,
        'search_delay' => 0,
        'map_scroll_wheel' => FALSE,
        'map_street_view' => FALSE,
        'responsive' => FALSE,
        'show_results_on_map' => TRUE,
        'display_map_info' => TRUE,
        'theme' => 'mondialrelay',
      ],
      'services' => ['default'],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $amount = $this->configuration['rate_amount'];
    // A bug in the plugin_select form element causes $amount to be incomplete.
    if (isset($amount) && !isset($amount['number'], $amount['currency_code'])) {
      $amount = NULL;
    }

    $form['rate_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rate label'),
      '#description' => $this->t('Shown to customers when selecting the rate.'),
      '#default_value' => $this->configuration['rate_label'],
      '#required' => TRUE,
    ];
    $form['rate_amount'] = [
      '#type' => 'commerce_price',
      '#title' => $this->t('Rate amount'),
      '#default_value' => $amount,
      '#required' => TRUE,
    ];
    $form['rate_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rate description'),
      '#description' => $this->t('Provides additional details about the rate to the customer.'),
      '#default_value' => $this->configuration['rate_description'],
    ];

    $form['mondial_relay_widget'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Widget settings'),
      '#tree' => TRUE,
      'brand' => [
        '#type' => 'textfield',
        '#title' => $this->t('Brand'),
        '#description' => $this->t('Number of "Point Relais" to send'),
        '#required' => TRUE,
        '#default_value' => $this->configuration['widget']['brand'] ?: '',
      ],
      'default_country' => [
        '#type' => 'select',
        '#title' => $this->t('Default country'),
        '#options' => ['' => $this->t('- None -')] + $this->countryRepository->getList(),
        '#default_value' => $this->configuration['widget']['default_country'] ?: NULL,
      ],
      'enable_geolocalisated_search' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable geolocalisated search'),
        '#description' => $this->t("Enable or disable the ability to search for the current position when the user's browser supports this function (browser request)."),
        '#default_value' => $this->configuration['widget']['enable_geolocalisated_search'] ?: 0,
      ],
      'col_liv_mod' => [
        '#type' => 'select',
        '#title' => $this->t('Package shipping mode'),
        '#description' => $this->t('Allows you to filter Relay Points according to the delivery mode used (Relay Point L (24R), XL (24L), XXL (24X)). It is also possible to filter Lockers ("APM" value). Please refer to your contract for more information.'),
        '#options' => [
          '24R' => $this->t('Relay Point L'),
          '24L' => $this->t('XL'),
          '24X' => $this->t('XXL'),
          'APM' => $this->t('Filter lockers'),
        ],
        '#default_value' => $this->configuration['widget']['col_liv_mod'] ?: '24R',
      ],
      'enable_geolocalisated_search' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable geolocalisated search'),
        '#description' => $this->t("Enable or disable the ability to search for the current position when the user's browser supports this function (browser request)."),
        '#default_value' => $this->configuration['widget']['enable_geolocalisated_search'] ?: 0,
      ],
      'nb_results' => [
        '#type' => 'number',
        '#title' => $this->t('Number of results'),
        '#description' => $this->t('Number of "Point Relais" to send'),
        '#default_value' => $this->configuration['widget']['nb_results'] ?: 7,
      ],
      'search_delay' => [
        '#type' => 'number',
        '#title' => $this->t('Search delay'),
        '#description' => $this->t('Allows you to specify the number of days between the search and drop-off of the parcel in our network. This option can be used to filter out Relay Points that may be on vacation at the time of delivery.'),
        '#default_value' => $this->configuration['widget']['search_delay'] ?: 0,
      ],
      'map_scroll_wheel' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Map scroll wheel'),
        '#description' => $this->t('Enable or disable zoom on scroll on the results map.'),
        '#default_value' => $this->configuration['widget']['map_scroll_wheel'] ?: 0,
      ],
      'map_street_view' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Map street view'),
        '#description' => $this->t('Enable or disable Street View mode on the results map (beware of quotas imposed by Google).'),
        '#default_value' => $this->configuration['widget']['map_street_view'] ?: 0,
      ],
      'responsive' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Responsive'),
        '#description' => $this->t('Enable Responsive?'),
        '#default_value' => $this->configuration['widget']['responsive'] ?: 0,
      ],
      'show_results_on_map' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Show results on map'),
        '#description' => $this->t('Enable or disable display of results on a map.'),
        '#default_value' => $this->configuration['widget']['show_results_on_map'] ?: 1,
      ],
      'display_map_info' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Display map info'),
        '#description' => $this->t('Enable or disable the display of a tooltip on the map when a Relay Point is selected.'),
        '#default_value' => $this->configuration['widget']['display_map_info'] ?: 1,
      ],
      'theme' => [
        '#type' => 'radios',
        '#title' => $this->t('Theme'),
        '#description' => $this->t('Determines the graphic theme to be used. The Mondial Relay theme will be used by default if this parameter is missing or incorrectly set. The two possible values are "mondialrelay" for Mondial Relay and "inpost" for Inpost (values must be entered without spaces and in lower case).'),
        '#options' => [
          'mondialrelay' => $this->t('Mondial Relay'),
          'inpost' => $this->t('Inpost'),
        ],
        '#required' => TRUE,
        '#default_value' => $this->configuration['widget']['theme'] ?: 'mondialrelay',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['rate_label'] = $values['rate_label'];
      $this->configuration['rate_description'] = $values['rate_description'];
      $this->configuration['rate_amount'] = $values['rate_amount'];
      $this->configuration['widget'] = $values['mondial_relay_widget'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment, $addressId = NULL) {
    // Set rates.
    $rates = [];
    $rates[] = new ShippingRate([
      'shipping_method_id' => $this->parentEntity->id(),
      'service' => $this->services['default'],
      'amount' => Price::fromArray($this->configuration['rate_amount']),
      'description' => $this->configuration['rate_description'],
    ]);

    return $rates;
  }

}
