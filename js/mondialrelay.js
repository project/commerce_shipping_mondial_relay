(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.commerce_shipping_mondial_relay = {
    attach: function (context) {
      $("#commerce_shipping_mondial_relay_widget").MR_ParcelShopPicker({
        Brand: drupalSettings.commerce_shipping_mondial_relay.widget.Brand,
        // Selecteur de l'élément dans lequel est envoyé l'ID du Point Relais (ex: input hidden)
        Target: "[data-drupal-selector=commerce-shipping-mondial-relay-pickup-id]",
        // Selecteur de l'élément dans lequel est envoyé l'ID du Point Relais pour affichage
        TargetDisplay: "#commerce_shipping_mondial_relay__pickup__selected",
        // Selecteur de l'élément dans lequel sont envoysé les coordonnées complètes du point relais
        TargetDisplayInfoPR: "#commerce_shipping_mondial_relay__pickup__info",
        OnParcelShopSelected: function (data) {
          $('[data-drupal-selector=commerce-shipping-mondial-relay-pickup-address-id]').val(data.ID);
          $('[data-drupal-selector=commerce-shipping-mondial-relay-pickup-address-nom]').val(data.Nom);
          $('[data-drupal-selector=commerce-shipping-mondial-relay-pickup-address-adresse1]').val(data.Adresse1);
          $('[data-drupal-selector=commerce-shipping-mondial-relay-pickup-address-adresse2]').val(data.Adresse2);
          $('[data-drupal-selector=commerce-shipping-mondial-relay-pickup-address-cp]').val(data.CP);
          $('[data-drupal-selector=commerce-shipping-mondial-relay-pickup-address-ville]').val(data.Ville);
          $('[data-drupal-selector=commerce-shipping-mondial-relay-pickup-address-pays]').val(data.Pays);
          $('#commerce_shipping_mondial_relay__pickup__hours').html(data.HoursHtmlTable);
        },
        AllowedCountries: drupalSettings.commerce_shipping_mondial_relay.widget.AllowedCountries,
        Country: drupalSettings.commerce_shipping_mondial_relay.widget.Country,
        PostCode: drupalSettings.commerce_shipping_mondial_relay.widget.PostCode,
        City: drupalSettings.commerce_shipping_mondial_relay.widget.City,
        AutoSelect: drupalSettings.commerce_shipping_mondial_relay.widget.AutoSelect,
        EnableGeolocalisatedSearch: drupalSettings.commerce_shipping_mondial_relay.widget.EnableGeolocalisatedSearch,
        ColLivMod: drupalSettings.commerce_shipping_mondial_relay.widget.ColLivMod,
        NbResults: drupalSettings.commerce_shipping_mondial_relay.widget.NbResults,
        SearchDelay: drupalSettings.commerce_shipping_mondial_relay.widget.SearchDelay,
        MapScrollWheel: drupalSettings.commerce_shipping_mondial_relay.widget.MapScrollWheel,
        MapStreetView: drupalSettings.commerce_shipping_mondial_relay.widget.MapStreetView,
        Responsive: drupalSettings.commerce_shipping_mondial_relay.widget.Responsive,
        ShowResultsOnMap: drupalSettings.commerce_shipping_mondial_relay.widget.ShowResultsOnMap,
        DisplayMapInfo: drupalSettings.commerce_shipping_mondial_relay.widget.DisplayMapInfo,
        Theme: drupalSettings.commerce_shipping_mondial_relay.widget.Theme,
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
